#include "EventWriterTera.h"

using namespace corryvreckan;

class TrackStateWriteMode : public EventWriterTera::WriteMode {
public:
    TrackStateWriteMode(const EventWriterTera& parent, double zIn, double zOut)
        : WriteMode(parent), mTriggerNumber(0), mXIn(0), mYIn(0), mZin(zIn), mXOut(0), mYOut(0), mZOut(zOut), mDXDZIn(0),
          mDYDZIn(0), mDXDZOut(0), mDYDZOut(0), mTree(nullptr) {}

    void initialize() override {
        mTree = new TTree("tracks", "tracks");
        mTree->Branch("triggerNr", &mTriggerNumber);
        mTree->Branch("x_in", &mXIn);
        mTree->Branch("y_in", &mYIn);
        mTree->Branch("z_in", &mZin);
        mTree->Branch("x_out", &mXOut);
        mTree->Branch("y_out", &mYOut);
        mTree->Branch("z_out", &mZOut);
        mTree->Branch("dxdz_in", &mDXDZIn);
        mTree->Branch("dydz_in", &mDYDZIn);
        mTree->Branch("dxdz_out", &mDXDZOut);
        mTree->Branch("dydz_out", &mDYDZOut);
    }

    StatusCode run(std::shared_ptr<Clipboard> const& cb) override {
        auto tracks = cb->getData<Track>();
        StatusCode result = StatusCode::NoData;

        if(!cb->isEventDefined() || cb->getEvent()->triggerList().empty()) {
            return result;
        }
        mTriggerNumber = static_cast<int>(cb->getEvent()->triggerList().begin()->first);

        for(auto track : tracks) {
            const auto intercept_in = track->getIntercept(mZin);
            const auto direction_in = track->getDirection(mZin);
            mXIn = intercept_in.X();
            mYIn = intercept_in.Y();
            mDXDZIn = direction_in.X() / direction_in.Z();
            mDYDZIn = direction_in.Y() / direction_in.Z();

            const auto intercept_out = track->getIntercept(mZOut);
            const auto direction_out = track->getDirection(mZOut);
            mXOut = intercept_out.X();
            mYOut = intercept_out.Y();
            mDXDZOut = direction_out.X() / direction_out.Z();
            mDYDZOut = direction_out.Y() / direction_out.Z();

            mTree->Fill();
            result = StatusCode::Success;
        }
        return result;
    }

    void finalize(std::shared_ptr<ReadonlyClipboard> const&) override {
        assert(mTree != nullptr);
        GetParent().getROOTDirectory()->WriteTObject(mTree, "tracks");
        delete mTree;
        mTree = nullptr;
    }

private:
    int mTriggerNumber;
    double mXIn;
    double mYIn;
    double mZin;
    double mXOut;
    double mYOut;
    double mZOut;
    double mDXDZIn;
    double mDYDZIn;
    double mDXDZOut;
    double mDYDZOut;

    TTree* mTree;
};

EventWriterTera::EventWriterTera(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors)
    : Module(config, detectors), mWriteMode(nullptr) {
    auto writeMode = config.get<std::string>(gWriteModeKey);

    double zIn = config.get<double>(gZInKey);
    double zOut = config.get<double>(gZOutKey);

    if(writeMode == gTrackStateWriteMode) {
        mWriteMode.reset(new TrackStateWriteMode(*this, zIn, zOut));
    }
    if(mWriteMode == nullptr) {
        throw std::invalid_argument("Unknown write mode");
    }
}

void EventWriterTera::initialize() {
    mWriteMode->initialize();
}

StatusCode EventWriterTera::run(std::shared_ptr<Clipboard> const& cb) {
    return mWriteMode->run(cb);
}

void EventWriterTera::finalize(std::shared_ptr<ReadonlyClipboard> const& cb) {
    mWriteMode->finalize(cb);
}
