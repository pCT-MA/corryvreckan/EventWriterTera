#pragma once

#include "core/module/Module.hpp"

namespace corryvreckan {
    class EventWriterTera : public Module {
    public:
        EventWriterTera(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors);
        virtual ~EventWriterTera() {}

        virtual void initialize() override;

        virtual StatusCode run(std::shared_ptr<Clipboard> const&) override;

        virtual void finalize(std::shared_ptr<ReadonlyClipboard> const&) override;

        class WriteMode {
        public:
            WriteMode(const EventWriterTera& parent) : mParent(parent) {}
            virtual void initialize() = 0;

            virtual StatusCode run(std::shared_ptr<Clipboard> const&) = 0;

            virtual void finalize(std::shared_ptr<ReadonlyClipboard> const&) = 0;

        protected:
            const EventWriterTera& GetParent() { return mParent; }

        private:
            const EventWriterTera& mParent;
        };

    private:
        static constexpr const char* gWriteModeKey = "write_mode";
        static constexpr const char* gTrackStateWriteMode = "track_state";

        static constexpr const char* gZInKey = "z_in";
        static constexpr const char* gZOutKey = "z_out";

        std::unique_ptr<WriteMode> mWriteMode;
    };
} // namespace corryvreckan
